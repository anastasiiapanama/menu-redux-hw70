import React from 'react';

import './OrderItem.css';

const OrderItem = props => {
    return (
        <div className="OrderItem">
            <div className="OrderImg">
                <img src={props.img} width="100px" height="100px"/>
            </div>
            <div className="OrderList">
                <div className="OrderInfo">
                    <h4>{props.name}</h4>
                    <p>Цена: <strong>{props.price} KGS</strong></p>
                </div>
                <button onClick={props.added}>Add to cart</button>
            </div>
        </div>
    );
};

export default OrderItem;