import React from 'react';

const CartOrder = props => {
    return (
        <div className="Cart-order">
            <h4>{props.name}</h4>
            <p> x {props.count}</p>
            <p>{props.cost}</p>
        </div>
    );
};

export default CartOrder;