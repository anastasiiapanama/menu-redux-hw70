import axios from "axios";

const axiosProducts = axios.create({
   baseURL: 'https://quotes-cw8-ponamareva-default-rtdb.firebaseio.com/'
});

export default axiosProducts;