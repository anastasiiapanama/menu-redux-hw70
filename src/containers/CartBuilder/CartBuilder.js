import React from 'react';
import {useDispatch, useSelector} from "react-redux";

import './CartBuilder.css';
import CartOrder from "../../components/CartOrder/CartOrder";

const CartBuilder = () => {
    const dispatch = useDispatch();
    const orders = useSelector(state => state.productsBuilder.products);
    const delivery = useSelector(state => state.productsBuilder.delivery);
    const totalPrice = useSelector(state => state.productsBuilder.totalPrice);
    console.log(orders)

    return (
        <div className="Cart-builder">
            {orders.map(id => (
                <CartOrder
                    key={id.id}
                    name={id.name}
                    count={id.count}
                    cost={id.cost}
                />
            ))}

            <div className="Card-info">
                <p>Доставка: <strong> {delivery} KGS</strong></p>
                <p>Итого: <strong>{totalPrice}</strong></p>
            </div>

            <button>Заказать</button>
        </div>
    );
};

export default CartBuilder;