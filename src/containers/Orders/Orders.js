import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchOrders} from "../../store/actions/ordersActions";
import OrderItem from "../../components/OrderItem/OrderItem";
import {addProducts, incrementCount} from "../../store/actions/productsBuilderActions";
import './Orders.css';

const Orders = () => {
    const dispatch = useDispatch();
    const orders = useSelector(state => state.orders.orders);

    useEffect(() => {
        dispatch(fetchOrders());
    }, [dispatch]);

    const addProductsHandler = order => {
        dispatch(addProducts(order));
    };

        return (
        <div className="OrderItem-block">
            {orders.map(order => (
                <OrderItem
                    key={order.id}
                    img={order.url}
                    name={order.name}
                    price={order.cost}
                    added={() => addProductsHandler(order)}
                />
            ))}
        </div>
    );
}
;

export default Orders;