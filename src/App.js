import React from 'react';
import Orders from "./containers/Orders/Orders";
import CartBuilder from "./containers/CartBuilder/CartBuilder";

import './App.css';

const App = () => (
    <>
        <div className="Order-block">
            <div className="Orders-item">
                <Orders />
            </div>
            <div className="CartBuilder">
                <CartBuilder />
            </div>
        </div>
    </>
);

export default App;
