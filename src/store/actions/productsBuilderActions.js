export const ADD_PRODUCTS = 'ADD_PRODUCTS';
export const REMOVE_PRODUCTS = 'REMOVE_PRODUCTS';
export const INCREMENT_COUNT = 'INCREMENT_COUNT'

export const addProducts = order => ({type: ADD_PRODUCTS, order});
export const removeProducts = id => ({type: REMOVE_PRODUCTS, id});
export const incrementCount = index => ({type: INCREMENT_COUNT, index})