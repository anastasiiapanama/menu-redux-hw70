import axiosProducts from "../../redux-products";

export const FETCH_ORDERS_REQUEST = 'FETCH_ORDERS_REQUEST';
export const FETCH_ORDERS_SUCCESS = 'FETCH_ORDERS_SUCCESS';
export const FETCH_ORDERS_FAILURE = 'FETCH_ORDERS_FAILURE';

export const fetchOrdersRequest = () => ({type: FETCH_ORDERS_REQUEST});
export const fetchOrdersSuccess = orders => ({type: FETCH_ORDERS_SUCCESS, orders});
export const fetchOrdersFailure = error => ({type: FETCH_ORDERS_FAILURE, error });

export const fetchOrders = () => {
  return async dispatch => {
      try {
          dispatch(fetchOrdersRequest());

          const response = await axiosProducts.get('products.json');
          const orders = Object.keys(response.data).map(id => ({
              ...response.data[id],
              id
          }));

          dispatch(fetchOrdersSuccess(orders));
      } catch (error) {
          dispatch(fetchOrdersFailure(error));
      }
  }
};