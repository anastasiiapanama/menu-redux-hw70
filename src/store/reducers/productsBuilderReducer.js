import {ADD_PRODUCTS} from "../actions/productsBuilderActions";

const initialState = {
    products: [],
    totalPrice: 0,
    delivery: 150
};

const productsBuilderReducer = (state = initialState, action) => {
    switch (action.type) {
        case ADD_PRODUCTS:
            const products = [...state.products];
            if(products.length) {
                for(let item of products) {
                    if(item.id === action.order.id) {
                        item.count += 1
                        return {
                            ...state,
                            products: products
                        }
                    }
                }
            }
            action.order.count = 1;
            return {
                ...state,
                products: state.products.concat(action.order),
            }
        default:
            return state;
    }
};

export default productsBuilderReducer;