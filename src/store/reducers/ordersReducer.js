import {
    FETCH_ORDERS_REQUEST,
    FETCH_ORDERS_SUCCESS,
    FETCH_ORDERS_FAILURE,
} from "../actions/ordersActions";

const initialState = {
    loading: false,
    error: null,
    orders: [],
    fetchLoading: false,
    fetchError: false,
};

const ordersReducer = (state = initialState, action) => {
    switch(action.type) {
        case FETCH_ORDERS_REQUEST:
            return {...state, fetchLoading: true};
        case FETCH_ORDERS_SUCCESS:
            return {...state, orders: action.orders, fetchLoading: false};
        case FETCH_ORDERS_FAILURE:
            return {...state, fetchLoading: false, fetchError: action.error};
        default:
            return state;
    }
};

export default ordersReducer;